package com.umsss.users.application.model.repository;

import com.umsss.users.application.model.domain.Account;
import com.umsss.users.application.model.domain.AccountState;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AccountRepository extends JpaRepository<Account, Long> {

    List<Account> findByEmail(String email);

    List<Account> findByEmailAndState(String email, AccountState state);
}
