package com.umsss.users.application.model.domain;

public enum AccountState {
    ACTIVATED,
    DEACTIVATED
}