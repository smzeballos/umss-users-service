package com.umsss.users.application.api;

import com.umsss.users.application.model.domain.AccountState;

public class AccountInput {

    private String email;

    private AccountState state;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public AccountState getState() {
        return state;
    }

    public void setState(AccountState state) {
        this.state = state;
    }
}
