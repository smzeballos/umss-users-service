package com.umsss.users.application.controller;

import com.umsss.users.application.api.StudentInput;
import com.umsss.users.application.model.domain.Account;
import com.umsss.users.application.model.domain.AccountState;
import com.umsss.users.application.model.domain.Student;
import com.umsss.users.application.model.repository.AccountRepository;
import com.umsss.users.application.model.repository.StudentRepository;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

import java.util.Date;

/**
 * @author Santiago Mamani
 */

@Api(
        tags = "Rest-students-controller",
        description = "Operations over students"
)
@RequestMapping(value = Constants.BasePath.STUDENTS)
@RestController
@RequestScope
public class StudentController {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private StudentRepository studentRepository;

    @RequestMapping(method = RequestMethod.POST)
    public Student createStudent(@RequestBody StudentInput input) {
        Account account = new Account();
        account.setEmail(input.getEmail());
        account.setState(AccountState.ACTIVATED);
        account.setCreatedDate(new Date());
        account = accountRepository.save(account);

        // Create student with FK

        Student student = new Student();
        student.setFirstName(input.getFirstName());
        student.setLastName(input.getLastName());
        student.setPassword(input.getPassword());
        student.setActive(Boolean.TRUE);
        student.setNote(input.getNote());
        student.setCreatedDate(new Date());

        student.setAccount(account);

        return studentRepository.save(student);
    }
}
