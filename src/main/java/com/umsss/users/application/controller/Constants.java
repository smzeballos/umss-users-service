package com.umsss.users.application.controller;

/**
 * @author Santiago Mamani
 */
public final class Constants {

    private Constants() {

    }

    public final class BasePath {
        public static final String STUDENTS = "/students";
    }
}
