package com.umsss.users.application.controller;

import com.umsss.users.application.api.AccountInput;
import com.umsss.users.application.model.Test;
import com.umsss.users.application.model.domain.Account;
import com.umsss.users.application.model.domain.AccountState;
import com.umsss.users.application.model.repository.AccountRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.RequestScope;

import java.util.Date;
import java.util.List;

@Api(
        tags = "Rest-account-controller",
        description = "Operations over accounts"
)
@RequestMapping(value = "/accounts")
@RestController
@RequestScope
public class AccountController {

    private Test test;

    @Autowired
    private AccountRepository repository;

    @Autowired
    public void setTest(Test test) {
        this.test = test;
    }

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public void testMethod() {
        test.print();
    }

    @ApiOperation(
            value = "Create an account"
    )
    @ApiResponses({
            @ApiResponse(code = 201, message = "Account created"),
            @ApiResponse(code = 404, message = "Not found account")
    })
    @RequestMapping(method = RequestMethod.POST)
    public Account createAccount(@RequestBody AccountInput input) {

        Account account = new Account();
        account.setEmail(input.getEmail());
        account.setState(input.getState());
        account.setCreatedDate(new Date());

        return repository.save(account);
    }

    @RequestMapping(value = "/{accountId}", method = RequestMethod.GET)

    public Account readAccount(@PathVariable("accountId") Long accountId) {
        return repository.findById(accountId).orElse(null);
    }

    @RequestMapping(method = RequestMethod.GET)

    public List<Account> findAccountByEmail(@RequestParam("email") String email) {
        return repository.findByEmail(email);
    }

    @RequestMapping(
            value = "/data",
            method = RequestMethod.GET)
    public List<Account> readAccountByEmailAndState(@RequestParam(value = "email", required = false) String email,
                                                    @RequestParam(value = "state") AccountState state) {
        return repository.findByEmailAndState(email, state);
    }

    @RequestMapping(
            value = "/{accountId}",
            method = RequestMethod.PUT)
    public Account updateAccount(@PathVariable("accountId") Long accountId,
                                 @RequestBody AccountInput input) {

        Account account = repository.findById(accountId).orElse(null);

        if (account != null) {
            account.setEmail(input.getEmail());
            account.setState(input.getState());

            return repository.save(account);
        }
        return account;
    }

    @RequestMapping(
            value = "/{accountId}",
            method = RequestMethod.DELETE)
    public void deleteAccount(@PathVariable(value = "accountId") Long accountId) {

        repository.deleteById(accountId);
    }

}
